import React, { useState, useRef, useEffect } from 'react';
import classes from './App.module.css';
import axios from '../../axios-firebase';

// Composants
import Task from '../../Components/Task/Task';

function App() {
	// State
	const [tasks, setTasks] = useState([]);

	const [formText, setFormText] = useState('');

	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		recupererTasks();
		taskInput.current.focus();
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	}, []);

	// Méthodes
	const removedClickHandler = (index) => {
		const newTasksList = [...tasks];
		newTasksList.splice(index, 1);
		setTasks(newTasksList);
		axios
			.delete('/task/' + tasks[index].id + '.json', newTasksList[index])
			.then((response) => {
				console.log(response);
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const validationClickHandler = (index) => {
		const newTasksList = [...tasks];
		newTasksList[index].validation = !newTasksList[index].validation;
		setTasks(newTasksList);
		axios
			.put('/task/' + tasks[index].id + '.json', newTasksList[index])
			.then((response) => {
				console.log(response);
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const handleTextChange = (e) => {
		setFormText(e.target.value);
	};

	const handleCreateTask = (e) => {
		if (formText) {
			const newTask = { intitule: formText, validation: false };
			axios
				.post('/task.json', newTask)
				.then((response) => {
					console.log(response);
					recupererTasks();
				})
				.catch((error) => {
					console.log(error);
				});
		}
		setFormText('');
		e.preventDefault();
	};

	const recupererTasks = () => {
		axios
			.get('/task.json')
			.then((response) => {
				const nouvellesTaches = [];
				for (const key in response.data) {
					nouvellesTaches.push({
						...response.data[key],
						id: key,
					});
				}
				setTasks(nouvellesTaches);
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const taskInput = useRef();

	const tasksList = tasks.map((task, index) => (
		<Task
			key={index}
			intitule={task.intitule}
			handleRemove={() => removedClickHandler(index)}
			handleValidation={() => validationClickHandler(index)}
			validation={task.validation}
		/>
	));

	return (
		<div className={classes.App}>
			<header>
				<span>TO-DO</span>
			</header>

			<div className={classes.add}>
				<form onSubmit={handleCreateTask}>
					<input
						type='text'
						ref={taskInput}
						value={formText || ''}
						placeholder='Que souhaitez-vous ajouter ?'
						onChange={handleTextChange}
					/>
					<button type='submit'>Ajouter</button>
				</form>
			</div>
			{tasksList}
		</div>
	);
}

export default App;
